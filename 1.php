<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'starter';
$db_name = 'pfig';

$link = mysql_connect($db_host, $db_user, $db_pass) or die('error');
mysql_select_db($db_name, $link) or die('error #2');

////////////////////////////////////////////////////////////////////////////////////

$q = "SHOW tables";
$res = mysql_query($q);

echo '<pre>';
while ($row = mysql_fetch_object($res)) {
  echo $row->Tables_in_pfig . PHP_EOL;
}
echo '</pre>';

////////////////////////////////////////////////////////////////////////////////////

$q = "CREATE TABLE IF NOT EXISTS `autorzy_kopia` (
        `id` INT(2) NOT NULL AUTO_INCREMENT,
        `imie` VARCHAR(255),
        `nazwisko` VARCHAR(255),
        `wiek` INT(2),
        PRIMARY KEY(`id`)
     );
";

mysql_query($q);

////////////////////////////////////////////////////////////////////////////////////

$q = "SELECT `k`.*, `w`.`nazwa` 
        FROM `ksiazki` `k` 
        JOIN `wydawnictwo` `w` ON `w`.`id` = `k`.`id_wydawnictwa`
        WHERE `w`.`id` > 0";
$res = mysql_query($q);

echo '<table width="100%" border=1>';
echo '<tr>
        <th>id</th>
        <th>imie</th>
        <th>nazwisko</th>
        <th>wiek</th>
        <th>wydawnictwo</th>
      </tr>';

while ($row = mysql_fetch_array($res)) {
  echo '<tr>';
    echo '<td>'.$row[0].'</td>';
    echo '<td>'.$row[1].'</td>';
    echo '<td>'.$row[2].'</td>';
    echo '<td>'.$row[3].'</td>';
    echo '<td>'.$row[4].'</td>';
  echo '</tr>';

  $row[3] = (int) $row[3];
  $row[2] = mysql_real_escape_string($row[2]);

  $q = "REPLACE INTO `autorzy_kopia` VALUES ({$row[0]}, '{$row[1]}', '{$row[2]}', {$row[3]})";
  if (mysql_query($q)) {
    echo 'dobrze '.PHP_EOL;
  } else {
    echo 'zle '.$row[2].PHP_EOL;
  }

}

echo '</table>';

/////////////////////////////////////////////////////////////////////////////////////

$q = 'SELECT COUNT(*) AS `suma`
        FROM `autorzy_kopia` 
        WHERE `id`>0 ';

$res = mysql_query($q);
$row = mysql_fetch_assoc($res);

print_r($row);

?>